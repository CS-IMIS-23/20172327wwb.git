
//***********************************************
//
//      Filename: NameTag.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-04-15 16:58:25
// Last Modified: 2018-04-15 16:58:25
//***********************************************
public class NameTag {
    public static void main(String[] args) {
        System.out.println();
        System.out.println("   " + args[0]);
        System.out.println("My name is " + args[1]);

    }
}
