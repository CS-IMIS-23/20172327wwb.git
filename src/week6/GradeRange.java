
//***********************************************
//
//      Filename: GradeRange.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-04-15 16:56:20
// Last Modified: 2018-04-15 16:56:20
//***********************************************
public class GradeRange {
    public static void main(String[] args) {
          Grade[] grades =
                  {
                          new Grade("A", 95), new Grade("A--", 90),
                          new Grade("B+", 87), new Grade("B", 85), new Grade("B-", 80),
                          new Grade("C+", 77), new Grade("C", 75), new Grade("C-", 70),
                          new Grade("D+", 67), new Grade("D", 65), new Grade("D-", 60),
                          new Grade("F", 0)
                  };
          for (Grade letterGrade : grades)
              System.out.println(letterGrade);


    }
}
