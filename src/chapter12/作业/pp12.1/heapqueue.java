package chapter12.作业;


import chapter12.jsjf.ArrayHeap;

public class heapqueue<T> extends ArrayHeap<heapObject<T>> {
    public heapqueue() {
        super();
    }

    public void enqueue(T object) {
        heapObject<T> obj = new heapObject<>(object);
        super.addElement(obj);
    }

    public T dequeue() {
        heapObject<T> obj = (heapObject<T>) super.removeMin();
        return obj.element;
    }
    public T first(){
        heapObject<T> o = (heapObject<T>) super.findMin();
        return o.getElement();
    }
    @Override
    public boolean isEmpty(){
        return super.isEmpty();
    }

    @Override
    public int size(){
        return super.size();
    }

    @Override
    public String toString(){
        return super.toString();
    }
}