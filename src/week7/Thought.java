
//***********************************************
//
//      Filename: Thought.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-04-21 11:41:50
// Last Modified: 2018-04-21 11:41:50
//***********************************************
public class Thought {
    public void message()
    {
        System.out.println("I feel like I'm diagonally parked in a " + "parallel universe.");
        System.out.println();
    }
}
