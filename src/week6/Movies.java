
//***********************************************
//
//      Filename: Movies.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-04-15 16:57:41
// Last Modified: 2018-04-15 16:57:41
//***********************************************
import java.text.DateFormat;

public class Movies {
    public static void main(String[] args) {
        DVDCollection movies = new DVDCollection();

        movies.addDVD("The Godfather","Francis Ford Coppola",1972,24.95,true);
        movies.addDVD("District 9","Neill Blomkamp",2009,19.95,false);
    }

