package chapter9.作业.实践;

import chapter6.实践.Product;
import chapter6.练习题.pp6_11.LinearNode;
import chapter6.练习题.pp6_11.LinkedList;
import chapter6.练习题.pp6_11.UnorderedListADT;
import chapter6.练习题.pp6_11.exceptions.ElementNotFoundException;

/**
 * LinkedUnorderedList represents a singly linked implementation of an 
 * unordered list.
 *
 * @author Lewis and Chase
 * @version 4.0
 */
public class LinkedUnorderedList<T> extends LinkedList<T>
         implements UnorderedListADT<T> {
    /**
     * Creates an empty list.
     */

    public LinkedUnorderedList() {
        super();
    }

    /**
     * Adds the specified element to the front of this list.
     *
     * @param element the element to be added to the list
     */
    @Override
    public void addToFront(T element) {
        LinearNode temp = head;
        LinearNode node = new LinearNode(element);
        node.setNext(temp);
        head = node;
        count++;
    }

    /**
     * Adds the specified element to the rear of this list.
     *
     * @param element the element to be added to the list
     */
    @Override
    public void addToRear(T element) {
        LinearNode temp = head;
        LinearNode node = new LinearNode(element);

        if (head==null)
        {
            head=node;
        }
        else {
            while (temp.getNext() != null) {
                temp = temp.getNext();
            }
            temp.setNext(node);
            count++;
        }
    }


    /**
     * Adds the specified element to this list after the given target.
     *
     * @param element the element to be added to this list
     * @param target  the target element to be added after
     * @throws ElementNotFoundException if the target is not found
     */
    @Override
    public void addAfter(T element, T target) {
        LinearNode node = new LinearNode(element);
        LinearNode temp = head;
        for (int i = 0; i < Integer.parseInt(String.valueOf(element)); i++) {
            temp = temp.getNext();
        }
        node.setNext(temp.getNext().getNext());
        temp.setNext(node);
        count++;
    }

    public boolean find(T element) {
        boolean found = false;
        //chapter6.实践.LinearNode<T> previous = null;
        LinearNode<T> current = head;
        while (current != null && !found) {
            if (element.equals(current.getElement())) {
                found = true;
                break;
            } else {
                current = current.getNext();
            }
        }
        return found;
    }


    public void selectionSort() {
        LinearNode<Product> temp = (LinearNode<Product>) head;
        LinearNode<Product> node1, node2, node3;
        node1 = node2 = node3 = null;
        Product min = new Product("", 0, 0, 0);
        while (temp.getNext() != null) {
            node2 = null;
            node3 = node1 = temp;
            while (node1.getNext() != null) {
                if (node1.getNext().getElement().getValue()<node3.getElement().getValue()) {
                    node3 = node2 = node1.getNext();
                    min = node1.getNext().getElement();
                }
                node1 = node1.getNext();
            }
            if (node2 != null) {
                node2.setElement(temp.getElement());
                temp.setElement(min);
            }
            temp = temp.getNext();
        }

    }

}


