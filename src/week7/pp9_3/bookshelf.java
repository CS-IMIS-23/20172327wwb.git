
//***********************************************
//
//      Filename: bookshelf.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-04-22 20:46:59
// Last Modified: 2018-04-22 20:46:59
//***********************************************
public class bookshelf {
    public static void main(String[] args) {
        Novel a = new Novel("The tragedy of the United States ",1500,"For the sake of peace in one's soul, the most important thing is truth." );
        Magazine b = new Magazine("People Weekly ",50,"In a joint statement to People Magazine, the couple said they'd always be best friends. ");
        AcademicJournal c = new AcademicJournal("scientific research ",150,"Scientific research is a continuous electronic publication officially approved=");
        textbook d = new textbook("Java Software Solutions",480,"java");
        System.out.println("Bookname: " + a.getName());
        System.out.println("pages: " + a.getPages());
        System.out.println("Keyword: " + a.getKeyword());
        System.out.println();
        System.out.println("Bookname: " + b.getName());
        System.out.println("pages: " + b.getPages());
        System.out.println("Keyword: " + b.getKeyword());
        System.out.println();
        System.out.println("Bookname: " + c.getName());
        System.out.println("pages: " + c.getPages());
        System.out.println("Keyword: " + c.getKeyword());
        System.out.println();
        System.out.println("Bookname: " + d.getName());
        System.out.println("pages: " + d.getPages());
        System.out.println("Keyword: " + d.getKeyword());
    }


}
