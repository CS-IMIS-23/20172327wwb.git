package chapter10.jsjf;

public class test {
    public static void main(String[] args) {


                BinaryTreeNode a = new BinaryTreeNode(1);
                BinaryTreeNode b = new BinaryTreeNode(2);
                BinaryTreeNode c = new BinaryTreeNode(3);
                BinaryTreeNode d = new BinaryTreeNode(4);
                BinaryTreeNode e = new BinaryTreeNode(5);

                LinkedBinaryTree aa = new LinkedBinaryTree(d.getElement());
                LinkedBinaryTree bb = new LinkedBinaryTree(e.getElement());
                LinkedBinaryTree cc = new LinkedBinaryTree(c.getElement(),bb,aa);
                LinkedBinaryTree dd = new LinkedBinaryTree(b.getElement(),aa,bb);
                LinkedBinaryTree ee = new LinkedBinaryTree(a.getElement(),dd,cc);




        System.out.println("输出树："+ee.toString());

        System.out.println("叶子个数："+ee.childnum(ee));
        System.out.println("层序遍历的结果：");
        ee.levelOrder(ee.root);
        System.out.println();
        System.out.println("得到结点的右子树（root)");
        System.out.println(ee.getRight());
        System.out.println("得到结点的右孩子（root)");
        System.out.println(ee.getRight().getRootElement());
        System.out.println("前序遍历输出为");
        ee.iteratorPreOrder();
        System.out.println();
        System.out.println("后序输出为：");
        ee.postOrder1(ee.root);
        System.out.println();
        System.out.println("验证树中是否包含2");
        System.out.println(ee.contains(2));
            }
}
