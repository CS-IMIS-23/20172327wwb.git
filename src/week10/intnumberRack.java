
//***********************************************
//
//      Filename: intnumberRack.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-05-19 23:44:54
// Last Modified: 2018-05-19 23:44:54
//***********************************************
public class intnumberRack {
    public static void main(String[] args) {
        intnumberList rack=new intnumberList();
        rack.add(new intnum(100));
        rack.add(new intnum(35));
        rack.add(new intnum(6));
        rack.add(new intnum(195));
        rack.add(new intnum(1200));
        System.out.println(rack);
        rack.selectsort();

        System.out.println();
        System.out.println(rack);
    }
}
