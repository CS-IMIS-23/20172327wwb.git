
//***********************************************
//
//      Filename: CreatingExceptions.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-05-13 19:36:35
// Last Modified: 2018-05-13 19:36:35
//***********************************************
import java.util.Scanner;

public class CreatingExceptions
{
   //-----------------------------------------------------------------
   //  Creates an exception object and possibly throws it.
   //-----------------------------------------------------------------
   public static void main(String[] args) throws OutOfRangeException,ArithmeticException
   {
      final int MIN = 25, MAX = 40;

      Scanner scan = new Scanner(System.in);

      OutOfRangeException problem =
         new OutOfRangeException("Input value is out of range.");
      AbstractMethodError problem1 = new AbstractMethodError();

      System.out.print("Enter an integer value between " + MIN +
                       " and " + MAX + ", inclusive: ");
      int value = scan.nextInt();

      //  Determine if the exception should be thrown
      if (value < MIN || value > MAX)
         throw problem;

      System.out.println("End of main method.");  // may never reach
   }
}

