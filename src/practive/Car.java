
//***********************************************
//
//      Filename: Car.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-03-29 20:08:17
// Last Modified: 2018-03-29 20:08:17
//***********************************************
public class Car
{
private String Name;
private String Model;
private int Time;
private int  IsAntique;
private final int Ture = 1;
private final int False = 0;
private int Year = 2018;
public Car(String name,String model,int time)
{
     Name = name;
     Model = model;
     Time = time;
    IsAntique = isAntique;
}
public void setName(String Name){
   Name = Name;}
public String getName(){
return Name;}
public void setModel(String Model){
 Model = Model;}
public String getModel(){
  return Model;}
public void setTime(int Time){
Time = Time;}
public int getTime(){
return Time;}
public void setIsAntique(int  IsAntique){
   if (Year - Time>45)
    IsAntique = Ture;
   else
    IsAntique = False;}
public int getIsAntique(){
  return IsAntique;}
public String toString() 
{
return"\nName: " + Name + "\nModel: " + Model + "\nTime: " + Time + "\nIsAntique: " + IsAntique;}
}
