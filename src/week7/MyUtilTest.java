
//***********************************************
//
//      Filename: MyUtilTest.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-04-21 11:58:49
// Last Modified: 2018-04-21 11:58:49
//***********************************************
import org.junit.Test;
import junit.framework.TestCase;

public class MyUtilTest extends TestCase {
  @Test
    public void testNormal() {

        //测试边界情况
      if(MyUtil.percentage2fivegrade(0) != "不及格")
          System.out.println("test failed 1!");
      else if(MyUtil.percentage2fivegrade(60) != "及格")
          System.out.println("test failed 2!");
      else if(MyUtil.percentage2fivegrade(70) != "中等")
          System.out.println("test failed 3!");
      else if(MyUtil.percentage2fivegrade(80) != "良好")
          System.out.println("test failed 4!");
      else if(MyUtil.percentage2fivegrade(90) != "优秀")
          System.out.println("test failed 5!");
      else if(MyUtil.percentage2fivegrade(100) != "优秀")
          System.out.println("test failed 6!");
      else
          System.out.println("test passed!");

    }

    @Test
     public void testBountdayr()
    {
        assertEquals("不及格",MyUtil.percentage2fivegrade(0));
        assertEquals("及格",MyUtil.percentage2fivegrade(60));
        assertEquals("中等",MyUtil.percentage2fivegrade(70));
        assertEquals("良好",MyUtil.percentage2fivegrade(80));
        assertEquals("优秀",MyUtil.percentage2fivegrade(90));
        assertEquals("优秀",MyUtil.percentage2fivegrade(100));
}
    @Test
public void testExciption()
    {if(MyUtil.percentage2fivegrade(-10) != "错误")
        System.out.println("test failed 1!");
    else if(MyUtil.percentage2fivegrade(115) != "错误")
        System.out.println("test failed 2!");
    else
        System.out.println("test passed!");

}

}

