
//***********************************************
//
//      Filename: die.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-03-28 00:17:14
// Last Modified: 2018-03-28 00:17:14
//***********************************************
 class Die
{
private final int MAX = 6;

private int faceValue;
public Die()
{
faceValue = 1;
}
public int roll()
{faceValue = (int)(Math.random() * MAX) + 1;
return faceValue;
}
public void setFaceValue(int value)
{faceValue = value;
}
public int getFacevalue()
{return faceValue;
}
public String toString()
{
String result = Integer.toString(faceValue);
return result;
}
public int getFaceValue() {
        return faceValue;
    }
}

