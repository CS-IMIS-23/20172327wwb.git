package exp;

import org.junit.Test;

import static org.junit.Assert.*;

public class MyComplexTest {
    static MyComplex com1 = new MyComplex(1, 1);
    static MyComplex com2 = new MyComplex(1, 1);
    static MyComplex com3 = new MyComplex(2, 2);
    static MyComplex com4 = new MyComplex(0 ,0);
    static MyComplex com5 = new MyComplex(1, 0);
    static MyComplex com6 = new MyComplex(0, 2);

    @Test
    public void equals() throws Exception{
        assertEquals(com1.equals(com2), true);
    }

    @Test
    public void complexAdd() throws Exception{
        assertEquals(com1.ComplexAdd(com2).toString(), com3.toString());
    }

    @Test
    public void complexSub() throws Exception{
        assertEquals(com1.ComplexSub(com2).toString(), com4.toString());
    }

    @Test
    public void complexMulti() throws Exception{
        assertEquals(com1.ComplexMulti(com2).toString(), com6.toString());
    }

    @Test
    public void complexDiv() throws Exception{
        assertEquals(com1.ComplexDiv(com2).toString(), com5.toString());
    }

}