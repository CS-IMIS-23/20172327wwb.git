
//***********************************************
//
//      Filename: Animal.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-04-23 09:24:39
// Last Modified: 2018-04-23 09:24:39
//***********************************************
public abstract class Animal {
    private String name;
    private int id;

    public Animal(String name, int id) {
        this.name = name;
        this.id = id;
    }
public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    public abstract void eat();
    public abstract void sleep();
    public abstract void introduction() ;
}
