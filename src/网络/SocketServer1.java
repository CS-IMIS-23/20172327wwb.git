
//***********************************************
//
//      Filename: SocketServer1.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-06-11 11:14:42
// Last Modified: 2018-06-11 11:14:42
//***********************************************
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.StringTokenizer;

/**
 * Created by besti on 2018/6/9.
 */
public class SocketServer1 {
    public static void main(String[] args) throws IOException {
        //1.建立一个服务器Socket(ServerSocket)绑定指定端口
        ServerSocket serverSocket=new ServerSocket(8800);
        //2.使用accept()方法阻止等待监听，获得新连接
        Socket socket=serverSocket.accept();
        //3.获得输入流
        InputStream inputStream=socket.getInputStream();
        BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(inputStream,"UTF-8"));
        //获得输出流
        OutputStream outputStream=socket.getOutputStream();
        PrintWriter printWriter=new PrintWriter(outputStream);
        //4.读取用户输入信息
        String info=null;
        while(!((info = bufferedReader.readLine()) ==null)){
            System.out.println("我是服务器，用户信息为：" + "排序前："+info);

        //给客户一个响应
        Sort a =new Sort();
        String str = info;
       String[] temp = str.split(" ");
       int q[] = new int[temp.length];
   for (int i=0;i<temp.length;i++){
       q[i]=Integer.parseInt(temp[i]);
   }

        String reply=a.selectionSort(q);
        printWriter.write(reply);
        printWriter.flush();}
        //5.关闭资源
        printWriter.close();
        outputStream.close();
        bufferedReader.close();
        inputStream.close();
        socket.close();
        serverSocket.close();
    }
}

