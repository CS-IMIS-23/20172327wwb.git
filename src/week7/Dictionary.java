
//***********************************************
//
//      Filename: Dictionary.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-04-21 11:44:41
// Last Modified: 2018-04-21 11:44:41
//***********************************************
public class Dictionary extends Book {
    private int definitions = 52500;

    public double computeRatio()
    {
        return (double)definitions/pages;
    }
    public void setDefinitions(int numDefinitions)
    {
        definitions = numDefinitions;
    }
    public int getDefinitions()
    {
        return definitions;
    }
}
