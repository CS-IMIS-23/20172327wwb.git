
//***********************************************
//
//      Filename: Complexity.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-04-03 17:16:12
// Last Modified: 2018-04-03 17:16:12
//***********************************************
public interface Complexity{
public void setComplexity(int complexity);
public int getComplexity();}
