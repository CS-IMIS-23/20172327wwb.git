
//***********************************************
//
//      Filename: Pizza.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-04-21 11:52:33
// Last Modified: 2018-04-21 11:52:33
//***********************************************
public class Pizza extends  FoodItem {
    public  Pizza(int fatGrams)
    {
        super(fatGrams,8);
    }
}
