
//***********************************************
//
//      Filename: Animalmain.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-04-23 09:25:03
// Last Modified: 2018-04-23 09:25:03
//***********************************************
public class Animalmain {
    public static void main(String[] args) {
        Cow a = new Cow("lalala",01);
        Sheep b = new Sheep("hey",01);

            a.introduction();
            a.eat();
            a.sleep();
            b.introduction();
            b.eat();
            b.sleep();
    }
}
