package chapter12.作业;

public class heapObject<T> implements Comparable<heapObject>{
    protected T element;
    protected int mark;
    protected int levelnum;
    private static int nextOrder = 0;
    public heapObject(T element) {
        this.element = element;
       // this.mark=mark;
        levelnum=nextOrder;
        nextOrder++;
    }

    public T getElement() {
        return element;
    }

    public int getMark() {
        return mark;
    }

    public int getLevelnum() {
        return levelnum;
    }

    public static int getNextOrder() {
        return nextOrder;
    }

    @Override
    public int compareTo(heapObject o) {
        int result=0;
        if (levelnum>o.getLevelnum()){
            result= 1;
        }
        else if (levelnum<o.getLevelnum()){
            result= -1;
        }
        else if (mark>o.getMark()){
            result=1;
        }
        else {
            result=-1;
        }
        return result;
    }

    @Override
    public String toString(){
        return element+" ";
    }
}
