
//***********************************************
//
//      Filename: pp8_1.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-04-15 13:33:54
// Last Modified: 2018-04-15 13:33:54
//***********************************************
import java.util.Scanner;

public class pp8_1 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        final int count = 51;
        int[] numbers = new int[count];
        int num;
        System.out.println("please enter a number: ");
        num = scan.nextInt();
        while (num >= 0 && num <= count) {

            numbers[num]++;
            System.out.println("please enter another number: ");
            num=scan.nextInt();
        }

        for (int index=0;index<51;index++)
        {
            System.out.println("number is :"+(index)+" times: "+numbers[index]);

        }
    }
}
