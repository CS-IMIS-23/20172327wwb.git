
//***********************************************
//
//      Filename: Account.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-04-03 16:35:44
// Last Modified: 2018-04-03 16:35:44
//***********************************************
import java.text.NumberFormat;
public class Account {
private long acctNumber;
private final double RATE = 0.035;
private double balance;
private String name;

public Account(String owner,long account,double initial){
name = owner;
acctNumber = account;
balance = initial;
}
public Account(String owner,long account)
{
name = owner;
acctNumber = account;
balance = 0.00;}
public double deposit(double amount){
balance = balance + amount;
return balance;
}
public double withdraw(double amount,double fee){
balance = balance - amount - fee;
return balance;}
public double addInterest(){
balance += (balance * RATE);
return balance;}
public double getBalance(){
return balance;}
public String toString()
{
NumberFormat fmt = NumberFormat.getCurrencyInstance();
return acctNumber + "\t" + name + "\t" + fmt.format(balance);
}
}
