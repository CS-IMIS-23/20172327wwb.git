
//***********************************************
//
//      Filename: average.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-04-16 09:17:47
// Last Modified: 2018-04-16 09:17:47
//***********************************************
public class average {
public static void main(string[] args){

    public double average(int ... list)
    {
        double result = 0.0;
        
        if(list.length!=0) {
            int sum = 0;
            for (int num : list)
                sum += num;
            result = (double) sum / list.length;
        
            }
            return result;
    }
    
}
}
