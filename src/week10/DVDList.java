
//***********************************************
//
//      Filename: DVDList.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-05-19 23:39:34
// Last Modified: 2018-05-19 23:39:34
//***********************************************
public class DVDList {
    private DVDNode list;

    public DVDList() {
        list = null;
    }
public void add(DVD dvd) {
    DVDNode node = new DVDNode(dvd);
    DVDNode current;
    if (list == null)
        list = node;
    else
    { current=list;
        while (current.next != null)
            current = current.next;
    current.next = node;}

}
@Override
public String toString()
{
    String result="";
    DVDNode current =list;
    while (current!=null)
    {
        result+=current.dvd+"\n";
        current=current.next;
    }
    return result;
}
    private class DVDNode {
        public DVD dvd;
        public DVDNode next;

        public DVDNode(DVD dv) {
            dvd = dv;
            next = null;
        }
    }
}
