
//***********************************************
//
//      Filename: pp12_1.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-05-13 21:34:04
// Last Modified: 2018-05-13 21:34:04
//***********************************************
import java.util.Arrays;
import  java.util.Scanner;

public class pp12_1 {
    public String palindromeJudge(String str) {

        if (str.length() == 1 || str.length() == 0) {
            return "That string IS a palindrome.";
        } else if (str.charAt(0) == str.charAt(str.length() - 1)) {

            String strShort = stringCut(str);
            return palindromeJudge(strShort);
        } else {
            return "That string is NOT a palindrome.";
        }
    }

    
    public static String stringCut(String str) {
        if (str == null || "".equals(str)) {
            return "";
        }
        char[] src = str.toCharArray();
        char[] dst = Arrays.copyOfRange(src, 1, src.length - 1);
        return String.valueOf(dst);
    }
    public static void main(String[] args) {
        String a;
        String str, another = "y";
        pp12_1 test = new pp12_1();
        Scanner scan = new Scanner(System.in);

        while (another.equalsIgnoreCase("y")) {
            System.out.println("Enter a potential palindrome: ");
            str = scan.nextLine();
            System.out.println(test.palindromeJudge(str));
            System.out.println();
            System.out.println("Test another paliandrome(y/n)?");
            another = scan.nextLine();
        }
    }
}

