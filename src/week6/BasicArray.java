
//***********************************************
//
//      Filename: BasicArray.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-04-15 16:50:17
// Last Modified: 2018-04-15 16:50:17
//***********************************************
public class BasicArray {
    public static void main(String[] args) {
        final int LIMIT = 15,MULTIPLE = 10;

        int[] list = new int[LIMIT];
        for (int index = 0;index<LIMIT;index++)
            list[index] = index*MULTIPLE;
        list[5] = 999;

        for (int value:list)
            System.out.print(value + " ");

        System.out.println(list.length);
    }
}
