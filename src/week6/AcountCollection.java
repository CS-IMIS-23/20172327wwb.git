
//***********************************************
//
//      Filename: AcountCollection.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-04-21 15:43:15
// Last Modified: 2018-04-21 15:43:15
//***********************************************
public class AcountCollection {
    private Account[] accounts;
    private int count;

    public AcountCollection() {
        accounts = new Account[30];
        count = 0;
    }

    /*设置一个账户*/
    public void addAcount(String owner, long account, double initial) {
        if (count <= 30) {
            System.out.println("您的账户名字为： " + owner);
            System.out.println("您的账户号码是： " + account);
            System.out.println("您的账户当前余额为： " + initial);
            count++;
        } else
            System.out.println("账户个数已满三十个，不能再添加新的账户。");
    }

    public String addmoney(double amount, int number) {
        if (accounts[number] != null) {
            accounts[number].deposit(amount);
            return "您要存入： " + amount + "存入的账户：" + number + "\n您的余额：" + accounts[number].getBalance();
        } else
            return "错误";
    }

    public String getmoney(double amount, double fee, int number) {
        if (accounts[number] != null) {
            accounts[number].withdraw(amount, fee);
            return "您要取： " + amount + "取出的账户： " + number + "\n您的给的小费： " + fee + "\n您的余额： " + accounts[number].getBalance();
        }
        else
            return "错误。";
    }

public String addinterest()
{
    for (int acctIndex = 0; acctIndex <= count; acctIndex++)
    {
        if (accounts[acctIndex] != null)
        {
            accounts[acctIndex].addInterest();
        }
        else
            break;
    }
    return "已经给每个加了利息。";
}
public String toString()
{
    String report = "账户的账号" + count;
    for (int acctIndex = 0; acctIndex <= count; acctIndex++)
    {
        if (accounts[acctIndex] != null)
            report += accounts[acctIndex].toString() + "\n";
        else
            break;
    }

    return report;

}
}

