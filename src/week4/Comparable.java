
//***********************************************
//
//      Filename: Comparable.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-04-03 21:03:47
// Last Modified: 2018-04-03 21:03:47
//***********************************************
public interface Comparable{
public void setComparable(int comparable);
public int getComparable();
}
