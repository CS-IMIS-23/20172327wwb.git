package chapter3.pp3_8;

public class StackTest {
    public static void main(String[] args) {
        ArrayStack a = new ArrayStack();
      a.push(99);
      a.push(66);
      a.push("ppp");
      a.size();
      System.out.println("当前栈的长度为： "+a.size());
      System.out.println("当前栈是否为空： "+a.isEmpty());
      System.out.println("栈的内容是： "+a.toString());
      System.out.println("当前栈的栈顶是： "+a.peek());
      a.pop();
      System.out.println(a.toString());
      }
}
