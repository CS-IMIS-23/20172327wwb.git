
//***********************************************
//
//      Filename: DigestPass.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-06-17 09:06:06
// Last Modified: 2018-06-17 09:06:06
//***********************************************
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class DigestPass {
    public DigestPass(){}
    public String Diget(String x) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        MessageDigest m=MessageDigest.getInstance("MD5");
        m.update(x.getBytes("UTF8"));
        byte s[ ]=m.digest( );
        String result="";
        for (int i=0; i<s.length; i++){
            result+=Integer.toHexString((0x000000ff & s[i]) |
                    0xffffff00).substring(6);
        }
        return result;
    }
}
