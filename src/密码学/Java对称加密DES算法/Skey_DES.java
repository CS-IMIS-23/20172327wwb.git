
//***********************************************
//
//      Filename: Skey_DES.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-05-20 16:02:24
// Last Modified: 2018-05-20 16:02:24
//***********************************************
import java.io.*;
import javax.crypto.*;
public class Skey_DES{
    public static void main(String args[])
            throws Exception{
        KeyGenerator kg=KeyGenerator.getInstance("DESede");
        kg.init(168);
        SecretKey k=kg.generateKey( );
        FileOutputStream  f=new FileOutputStream("key1.dat");
        ObjectOutputStream b=new  ObjectOutputStream(f);
        b.writeObject(k);
    }
}
