
//***********************************************
//
//      Filename: Dictionary2.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-04-21 11:46:50
// Last Modified: 2018-04-21 11:46:50
//***********************************************
public class Dictionary2 extends Book2{
      private int definitions;

      public Dictionary2(int numPages,int numDefinitions){
      super(numPages);

      definitions = numDefinitions;
      }
      public double computeRatio()
      {
          return (double) definitions;
      }
      public void setDefinitions(int numDefinitions)
      {
          definitions = numDefinitions;
      }
      public int getDefinitions()
      {
          return definitions;
      }
  }
