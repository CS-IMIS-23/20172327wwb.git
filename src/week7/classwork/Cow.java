
//***********************************************
//
//      Filename: Cow.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-04-23 09:25:26
// Last Modified: 2018-04-23 09:25:26
//***********************************************
public class Cow extends Animal {
    public Cow(String name, int id) {
        super(name, id);
    }

    @Override
    public void eat() {
      System.out.println(name+ " wanna to play.");
    }

    @Override
    public void sleep() {
        System.out.println(name+" want to go.");

    }

    @Override
    public void introduction() {
        System.out.println("my name is "+name+"my id is "+id);
    }
}

