package 第二次实验.第三个实验;

import java.io.FileNotFoundException;

/**
 * BackPainAnaylyzer demonstrates the use of a binary decision tree to 
 * diagnose back pain.
 */
public class BackPainAnalyzer
{
    /**
     *  Asks questions of the user to diagnose a medical problem.
     */
    public static void main (String[] args) throws FileNotFoundException
    {
        System.out.println ("论学Java重要还是女朋友重要");

        DecisionTree expert = new DecisionTree("Java");
        expert.evaluate();
        System.out.println(expert.toString());
        System.out.println("叶子结点有："+expert.childnum());
        System.out.println("深度为："+expert.height());
        System.out.println("非递归版本：");
        expert.feidiguilevel();
        System.out.println("递归版本：");
        expert.leveldigui();
    }
}
