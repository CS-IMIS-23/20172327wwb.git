
//***********************************************
//
//      Filename: DVD.java
//
//        Author: wwb
//        Mails: 1274860621@qq.com
//        Create: 2018-05-19 23:38:20
// Last Modified: 2018-05-19 23:38:20
//***********************************************
import java.text.NumberFormat;

public class DVD  {
    private String tirle ,director;
    private int year;
    private double cost;
    private boolean bluray;

    public DVD(String tirle,String director,int year,double cost,boolean bluray)
    {
        this.tirle = tirle;
        this.director = director;
        this.year = year;
        this.cost = cost;
        this.bluray = bluray;
    }
    @Override
    public String toString()
    {
        NumberFormat fmt = NumberFormat.getCurrencyInstance();
        String description;

        description = fmt.format(cost) + "\t" + year + "\t";
        description += tirle + "t" + "Blu-ray";

        return  description;
    }


    public String getTirle() {
        return tirle;
    }

    public String getDirector() {
        return director;
    }
}
